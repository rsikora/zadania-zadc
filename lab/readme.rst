Laboratorium #C
===============

1. Korzystając z klasy ``MapReduce`` zaimplementować w pliku ``join.py``
   odwzorowanie relacyjnego joina dla kwerendy

.. sourcecode:: SQL

    SELECT *
    FROM Orders, LineItem
    WHERE Order.order_id = LineItem.order_id

Dane wejściowe w postaci wierszy zakodowanych w formacie json znajdują się
w pliku ``records.json``. W każdym wierszu na pozycji zerowej znajduje się
nazwa zbioru, z którego on pochodzi (*order* lub *line_item*),
a na pozycji pierwszej identyfikator zamówienia (*order_id*).
Wyniki można zweryfikować z danymi znajdującymi się w pliku ``join.py``.


2. Pobrać dane z pliku ``/var/log/apache2/access.log`` znajdującym się na serwerze
   laboratoryjnym i zasilić nimi własną bazę na serwerze *CouchDB*. Następnie stworzyć
   widok pokazujący dla danego IP liczbę połączeń oraz ilość pobranych danych.
   Do parsowania logów można użyć funkcji ``parse`` z modułu ``parser``.
