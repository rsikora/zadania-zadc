import sys
import apachelog
import couchdb

couch = couchdb.Server('http://194.29.175.241:5984')
db = couch["p1"]

def parse(filename):
    format = r'%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"'
    parser = apachelog.parser(format)

    with open(filename, 'r') as file:
        for line in file:
            try:
                yield parser.parse(line)
            except:
                continue
                #sys.stderr.write("Problem z parsowaniem %s" % line)


def dbEntryCounter(db):
    counter = 0
    for id in db:
        counter+=1
    return counter

def main():
    while True:
        entries = parse("/var/log/apache2/access.log")
        dbEntriesCount = dbEntryCounter(db)
        logEntriesCount = len(list(entries))

        print  dbEntriesCount,logEntriesCount
        counter = 0

        for entry in entries:
            if dbEntriesCount < logEntriesCount:
                if counter > dbEntriesCount:
                    db.save({'ip': entry['%h'],'bytes':int(entry['%b'])})
                    dbEntriesCount = dbEntryCounter(db)
                else:
                    counter+=1
            else:
                continue

if __name__ == "__main__":
    main()